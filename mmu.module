<?php

/**
 * @file
 * Primary module hooks for Mix Modules Up module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme().
 */
function mmu_theme() {
  return [
    'mmu_modules_item' => [
      'render element' => 'row',
      'function' => 'theme_mmu_modules_item',
    ],
  ];
}

/**
 * Returns HTML for a single module item.
 */
function theme_mmu_modules_item($vars) {
  $module = $vars['row'];

  $machine_name = $module['#machine_name'];
  $renderer = Drupal::service('renderer');

  // Add the checkbox into the first cell.
  unset($module['enable']['#title']);
  $module['#requires'] = array_filter($module['#requires']);
  $module['#required_by'] = array_filter($module['#required_by']);

  $item_classes[] = 'mmu-mix';
  $item_classes[] = HTML::getClass('mmu-package-' . $module['#module']->info['package']);
  $item_classes[] = 'mmu-status-' . ($module['#module']->status ? 'enabled' : 'disabled');

  if (!empty($module['#module']->origin)) {
    $item_classes[] = 'mmu-source-core';
  }
  elseif (isset($module['#module']->info['project'])) {
    $item_classes[] = 'mmu-source-contrib';
  }
  else {
    $item_classes[] = 'mmu-source-custom';
  }

  $attributes['class'] = $item_classes;
  $attributes['data-package'] = $module['#module']->info['package'];
  $attributes['data-name'] = $module['#module']->info['name'];
  $attributes['data-machine-name'] = $machine_name;
  $attributes['data-status'] = $module['#module']->status ? 'enabled' : 'disabled';

  $dialog_id = Html::getUniqueId('mmu-dialog-' . $machine_name);

  $output = '<div' . new Attribute($attributes) . '>';

  $module['enable']['#title'] = $renderer->render($module['name']);
  $output .= '<div class="mmu-mix-label clearfix">' . $renderer->render($module['enable']) . '</div>';

  $description = Unicode::truncate($module['#module']->info['description'], 85, TRUE, TRUE);
  $output .= '<div class="mmu-mix-short-description" title="' . $module['#module']->info['description'] . '" data-dialog-id="' . $dialog_id . '">' . $description . '</div>';

  $details['Machine name'] = $machine_name;

  $details['Package'] = $module['#module']->info['package'];

  if (!empty($module['version']['#markup'])) {
    $version = $renderer->render($module['version']);
    $details['Version'] = $version;
  }
  if (!empty($module['#module']->info['mtime'])) {
    $details['Date'] = \Drupal::service('date.formatter')->format($module['#module']->info['mtime']);
  }
  if (!empty($module['#requires'])) {
    $details['Requires'] = implode(', ', $module['#requires']);
  }
  if (!empty($module['#required_by'])) {
    $details['Required by'] = implode(', ', $module['#required_by']);
  }

  $dialog = '';
  foreach ($details as $key => $value) {
    $dialog .= '<div class="mmu-dialog-detail"><b>' . $key . ':</b> ' . $value . '</div>';
  }

  $links = '';
  foreach (['help', 'permissions', 'configure'] as $link) {
    $links .= $renderer->render($module['links'][$link]);
  }

  if (isset($module['#module']->info['project'])) {
    $project_url = 'https://www.drupal.org/project/' . $module['#module']->info['project'];
    $project_link_attributes = [
      'href' => $project_url,
      'class' => ['module-link', 'module-link-project-page'],
      'title' => $project_url,
      'target' => '_blank',
    ];
    $links .= '<a' . new Attribute($project_link_attributes) . '>' . t('Project page') . '</a>';
  }

  if ($links) {
    $dialog .= '<div class="mmu-dialog-links">' . $links . '</div>';
  }

  $dialog_attributes = [
    'id' => $dialog_id,
    'class' => ['mmu-mix-dialog', 'hidden'],
    'title' => $module['#module']->info['name'],
  ];
  $output .= '<div' . new Attribute($dialog_attributes) . '>' . $dialog . '</div>';

  $output .= '</div>';

  return $output;
}
